!! Copyright (C) <2018>  <Dr Keith Evans : University of Nottingham>

!! This program is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.

!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.

!! You should have received a copy of the GNU General Public License
!! along with this program.  If not, see <https://www.gnu.org/licenses

MODULE SIMPLE_MATHS
  USE KINDS
  IMPLICIT NONE
CONTAINS
  REAL(KIND=RKIND) FUNCTION FACTORIAL(X)
    INTEGER(KIND=IKIND),INTENT(IN) :: X
    REAL(KIND=RKIND) :: DUMMY
    INTEGER(KIND=IKIND) :: ind
    DUMMY = 1
    DO ind = X,1,-1
       DUMMY = DUMMY*ind
    END DO
    FACTORIAL=DUMMY
  END FUNCTION FACTORIAL
END MODULE SIMPLE_MATHS

