!! Copyright (C) <2018>  <Dr Keith Evans : University of Nottingham>

!! This program is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.

!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.

!! You should have received a copy of the GNU General Public License
!! along with this program.  If not, see <https://www.gnu.org/licenses

SUBROUTINE Sensitivity(RPatients,RMonteCarlo,ILabels,RSensitivity,RSpecificity,RCutoffs,IMonteSize,K,INumPatients,ISuccess,IERR)
  USE KINDS
  IMPLICIT NONE
  
  REAL :: RTP,RTN,RFP,RFN
  INTEGER(KIND=IKIND),DIMENSION(:),ALLOCATABLE :: IClassification
  INTEGER,INTENT(OUT) :: ISuccess
  INTEGER(KIND=IKIND) :: ind,jnd,knd !Iterators
  INTEGER(KIND=IKIND),INTENT(IN) :: IMonteSize,K,INumPatients
  INTEGER(KIND=IKIND),INTENT(INOUT) :: IERR
  
  REAL(KIND=RKIND),INTENT(OUT) :: RSensitivity(IMonteSize),RSpecificity(IMonteSize) !,RY_hat
  REAL(KIND=RKIND),INTENT(OUT) :: RCutoffs(IMonteSize,K)
  REAL(KIND=RKIND),DIMENSION(:,:),INTENT(IN) :: RMonteCarlo(IMonteSize,K),RPatients(INumPatients,K)

  INTEGER(KIND=IKIND),INTENT(IN) :: ILabels(INumPatients)
  
  ISuccess = 0
  
  RTP = 0
  RFP = 0
  RFN = 0
  RTN = 0
  
  ALLOCATE(IClassification(INumPatients),STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "Error on Allocate of IClassification"
  END IF
  
  DO ind = 1,IMonteSize
     IClassification = 0  
     
     RTP = 0
     RFP = 0
     RFN = 0
     RTN = 0  
     DO knd = 1,K
        DO jnd = 1,INumPatients
           IF (RPatients(jnd,knd).GT.RMonteCarlo(ind,knd)) THEN
              IClassification(jnd) = 1
           END IF
        END DO
     END DO
     DO jnd = 1,INumPatients
        IF (IClassification(jnd).EQ.1) THEN
           IF (ILabels(jnd).EQ.1) THEN
              RTP = RTP + 1
           ELSE
              RFP = RFP + 1
           END IF
         ELSE
            IF (ILabels(jnd).NE.1) THEN
               RTN = RTN + 1
            ELSE
               RFN = RFN + 1
           END IF
        END IF
     END DO

     IF((RFN.LT.RTP).AND.(RFP.LT.((2.0/3.0)*RTN))) THEN
        ISuccess = ISuccess + 1
        RSensitivity(ISuccess) = RTP / (RTP + RFN)
        RSpecificity(ISuccess) = RTN / (RTN + RFP)
        RCutoffs(ISuccess,:) = RMonteCarlo(ind,:)
     END IF
     
  END DO
  DEALLOCATE(IClassification,STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "Error on DeAllocate of IClassification"
  END IF
  RETURN
END SUBROUTINE Sensitivity
