!! Copyright (C) <2018>  <Dr Keith Evans : University of Nottingham>

!! This program is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.

!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.

!! You should have received a copy of the GNU General Public License
!! along with this program.  If not, see <https://www.gnu.org/licenses

MODULE IO
  USE MPI
  USE KINDS
  IMPLICIT NONE
CONTAINS
  SUBROUTINE READINPUT(N,K,IMonteCarloIterations,IPatients, &
       MINSEN,MINSPEC,MINFILES,&
       RESTART,FILENAME,&
       RAntigen_measurements,ILabels,RestartMatrix,MY_RANK,IERR)
    
    INTEGER(KIND=IKIND),INTENT(OUT) :: N,K,IMonteCarloIterations,MINFILES,IPatients
    INTEGER(KIND=IKIND),DIMENSION(:),ALLOCATABLE,INTENT(OUT) :: ILabels
    REAL,INTENT(OUT) :: MINSEN
    REAL,INTENT(OUT) :: MINSPEC
    REAL(KIND=RKIND),DIMENSION(:,:),ALLOCATABLE,INTENT(OUT) :: RAntigen_measurements
    INTEGER(KIND=IKIND),DIMENSION(:),ALLOCATABLE,INTENT(OUT) :: RestartMatrix
    LOGICAL,INTENT(IN) :: RESTART
    
    INTEGER,INTENT(INOUT) :: IERR
    
    INTEGER,INTENT(IN) :: MY_RANK
    
    REAL :: Dummy
    CHARACTER(LEN=32),INTENT(INOUT) :: FILENAME
    INTEGER :: ind,IO
    !! Read param file
    
    OPEN(1,FILE=TRIM(FILENAME)//'.param',IOSTAT=IO)
    IF (IO.ne.0) THEN
       write(*,*) "(",MY_RANK,") Error on .param open"
    ENDIF
    
    DO ind = 1,6
       READ(1,*,IOSTAT=IO) Dummy
       IF (ind.eq.1) THEN
          N = NINT(Dummy)
       ELSE IF (ind.eq.2) THEN
          K = NINT(Dummy)
       ELSE IF (ind.eq.3) THEN
          IMonteCarloIterations = NINT(Dummy)
       ELSE IF (ind.eq.4) THEN
          MINSEN = Dummy
       ELSE IF (ind.eq.5) THEN
          MINSPEC = Dummy
       ELSE IF (ind.eq.6) THEN
          MINFILES = NINT(Dummy)
       ELSE
       END IF
    END DO
    CLOSE(1)
    
    IF(MY_RANK.EQ.0) THEN
       
       !! READ INPUT
       
       OPEN(1,FILE=TRIM(FILENAME)//'.labels',IOSTAT=IO)
       IF (IO.ne.0) THEN
          write(*,*) "(",MY_RANK,") Error on .labels open"
       ENDIF
       IPatients = 0
       DO
          READ(1,*,IOSTAT=IO)
          IF(IO.NE.0)EXIT
          IPatients = IPatients+1
       END DO
       CLOSE(1)
       CALL MPI_BCAST(IPatients,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on IPatients Communication"
       END IF
       
       ALLOCATE(RAntigen_Measurements(IPatients,N),STAT=IERR)
       IF (IERR.ne.0) THEN
          write(*,*) "(",MY_RANK,") Error on Antigen_Measurements ALLOCATE"
       ENDIF
       
       ALLOCATE(ILabels(IPatients),STAT=IERR)
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on Allocate of ILabels"
       END IF
       
       OPEN(1,FILE=TRIM(FILENAME)//'.labels',IOSTAT=IO)     
       IF (IO.ne.0) THEN
          write(*,*) "(",MY_RANK,") Error on .labels open"
       ENDIF
       
       DO ind = 1,IPatients
          READ(1,*,IOSTAT=IO) ILabels(ind)
          IF(IO.NE.0)EXIT
       END DO
       CLOSE(1)
       
       CALL MPI_BCAST(ILabels,IPatients,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on ILabels Communication"
       END IF
       
       OPEN(2,FILE=TRIM(FILENAME)//'.antigens',IOSTAT=IO)     
       IF (IO.ne.0) THEN
          write(*,*) "(",MY_RANK,") Error on .antigens open"
       ENDIF
       
       DO ind = 1,IPatients
          READ(2,*,IOSTAT=IO) RAntigen_Measurements(ind,:)
          IF(IO.NE.0)EXIT
       END DO
       CLOSE(2)
       
       DO ind = 1,IPatients
          CALL MPI_BCAST(RAntigen_Measurements(ind,:),N,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
          IF (IERR.NE.0) THEN
             WRITE(*,*) "(",MY_RANK,") Error on RAntigens Communication"
          END IF
       END DO
    ELSE
       CALL MPI_BCAST(IPatients,1,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on IPatients Communication"
       END IF
       
       ALLOCATE(RAntigen_Measurements(IPatients,N),STAT=IERR)
       IF (IERR.ne.0) THEN
          write(*,*) "(",MY_RANK,") Error on Antigen_Measurements ALLOCATE"
       ENDIF
       
       ALLOCATE(ILabels(IPatients),STAT=IERR)
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on Allocate of ILabels"
       END IF
       
       CALL MPI_BCAST(ILabels,IPatients,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on ILabels Communication"
       END IF
       
       DO ind = 1,IPatients
          CALL MPI_BCAST(RAntigen_Measurements(ind,:),N,MPI_DOUBLE_PRECISION,0,MPI_COMM_WORLD,IERR)
          IF (IERR.NE.0) THEN
             WRITE(*,*) "(",MY_RANK,") Error on RAntigens Communication"
          END IF
       END DO
    END IF
    
    !! Make Directories
    
    IF(RESTART) THEN
       WRITE(*,*) "Restart Mode Engaged!"
       ALLOCATE(RestartMatrix(4),STAT=IERR) !Restart Matrix is always 4 elements
       IF (IERR.NE.0) THEN
          WRITE(*,*) "(",MY_RANK,") Error on Allocate of ILabels"
       END IF
       write (FILENAME,"(A13,I4.4)") "Restart/Save_",MY_RANK 
       OPEN(10,FILE=TRIM(FILENAME),IOSTAT=IO)
       IF (IO.ne.0) THEN
          write(*,*) "(",MY_RANK,") Error on .RESTART! open"
       ENDIF
       READ(10,FMT='(I20,1X,I20,1X,I4,1X,I4)') RestartMatrix(:)
       CLOSE(1)
    ELSE
       IF(MY_RANK.EQ.0) THEN
          CALL execute_command_line(trim("mkdir -p Results"))
          CALL execute_command_line(trim("mkdir -p Restart"))
          CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
       ELSE
          CALL MPI_BARRIER(MPI_COMM_WORLD,IERR)
       END IF
    END IF
    
    
  END SUBROUTINE READINPUT
END MODULE IO
