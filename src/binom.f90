!! Copyright (C) <2018>  <Dr Keith Evans : University of Nottingham>

!! This program is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.

!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.

!! You should have received a copy of the GNU General Public License
!! along with this program.  If not, see <https://www.gnu.org/licenses/>.

module binom
  USE KINDS
  USE SIMPLE_MATHS
  implicit none
  INTEGER(IKIND) :: mm(100,100)

contains

   
   INTEGER(KIND=IKIND) FUNCTION NCHOOSEK(N,K)
     INTEGER(KIND=IKIND),INTENT(IN) :: N,K
     REAL(KIND=RKIND) :: DUMMY
     IF (k == 0) THEN
        DUMMY = 1
     ELSE IF (k == N) THEN
        DUMMY = 1
     ELSE IF (k == 1) THEN
        DUMMY = n
     ELSE IF (N.LT.K) THEN
        DUMMY = 0
     ELSE
        DUMMY = FACTORIAL(N)/(FACTORIAL(K)*FACTORIAL(N-K))
     END IF
     NCHOOSEK = NINT(DUMMY,IKIND)
   END FUNCTION NCHOOSEK  
end module binom
