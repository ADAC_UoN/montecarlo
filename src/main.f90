!! Copyright (C) <2018>  <Dr Keith Evans : University of Nottingham>

!! This program is free software: you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation, either version 3 of the License, or
!! (at your option) any later version.

!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.

!! You should have received a copy of the GNU General Public License
!! along with this program.  If not, see <https://www.gnu.org/licenses

program Antigens
  USE MPI
  USE BINOM
  USE KINDS
  USE IO
  
  IMPLICIT NONE
  
  INTEGER :: PROCESSES,MY_RANK,IERR,i,IWrite,IMAXLOC
  REAL::MINSPEC,MINSEN
  INTEGER(KIND=IKIND) :: N,K,ICount_local,ICount_global,ISuccessRate,ISuccess,Start,MINFILES
  REAL(KIND=RKIND) :: start_time, end_time, time_local,time_global
     
  INTEGER(KIND=IKIND) :: ind,jnd,knd ! iterators
  INTEGER(KIND=IKIND),DIMENSION(:),ALLOCATABLE :: ILargest_Vals,ICombination,RestartMatrix
  INTEGER(KIND=IKIND),DIMENSION(:,:),ALLOCATABLE :: ILookup_Table
  
  INTEGER(KIND=IKIND) :: IMaxcombinations,ILowerlim,IProgress,ICombinationSum
  INTEGER(KIND=IKIND) :: IMonteCarloIterations,IPatients !! Input Variables

  REAL(KIND=RKIND),DIMENSION(:,:),ALLOCATABLE :: RAntigen_Measurements,RMonteCarloArray,RMonteCarloArray2
  REAL(KIND=RKIND),DIMENSION(:),ALLOCATABLE :: RVectorMin,RScan_Matrix,RVectorMax,RRandomCol,RRandomRow
  
  REAL(KIND=RKIND),DIMENSION(:),ALLOCATABLE :: RSensitivity,RSpecificity !,RY_hat
  REAL(KIND=RKIND),DIMENSION(:,:),ALLOCATABLE :: RCutoffs
  
  INTEGER(KIND=IKIND),DIMENSION(:),ALLOCATABLE :: ILabels

  CHARACTER(LEN=32) :: FILENAME,arg
  CHARACTER(LEN=1024) :: OUTFILE,COMBINATIONFORMAT
  LOGICAL :: RESTART
  IERR = 0

  RESTART = .FALSE.
  !! MPI CALLS

  CALL MPI_INIT(IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on MPI_INIT"
  ENDIF
  
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,MY_RANK,IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on MPI_COMM_RANK"
  ENDIF
  
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,PROCESSES,IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on MPI_COMM_SIZE"
  ENDIF
  
  !! TIMING

  CALL CPU_TIME(start_time)
  
  !! Read Arguments

  i = 0
  
  DO
     CALL GET_COMMAND_ARGUMENT(i,arg)
     IF (LEN_TRIM(arg)==0) EXIT
     i = i + 1
     IF (i.eq.2) THEN
        FILENAME = ARG
     ELSE IF (i.eq.3) THEN
        RESTART=.TRUE.
     ELSE
     END IF
  END DO
  
  if(MY_RANK.EQ.0) THEN
     WRITE(*,*) "READING INPUT"
  END if
  
  CALL READINPUT(N,K,IMonteCarloIterations,IPatients,&
       MINSEN,MINSPEC,MINFILES,&
       RESTART,FILENAME,&
       RAntigen_measurements,ILabels,RestartMatrix,MY_RANK,IERR)
  
  if(MY_RANK.EQ.0) THEN
     WRITE(*,*) "Total Antigens         :",N
     WRITE(*,*) "Minimum Antigens       :",K
     WRITE(*,*) "Monte Carlo Iterations :",IMonteCarloIterations
     WRITE(*,*) "Patients               :",IPatients
     WRITE(*,*) "Minimum Sensitivity    :",MINSEN
     WRITE(*,*) "Minimum Specificity    :",MINSPEC
     WRITE(*,*) "Output Files Per Core  :",MINFILES
  END if
  
  
  !! ALLOCATE MEMORY

  ALLOCATE(ILookup_Table(N,K),STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on LOOKUP ALLOCATE"
  ENDIF
  
  ALLOCATE(ILargest_Vals(K),STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on Largest Vals ALLOCATE"
  ENDIF
  
  ALLOCATE(ICombination(K),STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on ICombinations ALLOCATE"
  ENDIF
  
  ALLOCATE(RVectorMin(N),STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RVectorMin ALLOCATE"
  ENDIF

  ALLOCATE(RVectorMax(N),STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RVectorMax ALLOCATE"
  ENDIF

  ALLOCATE(RScan_Matrix(IPatients),STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RScan_Matrix ALLOCATE" 
  ENDIF

  ALLOCATE(RMonteCarloArray(IMonteCarloIterations,100*K),STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RMonteCarloArray ALLOCATE"
  ENDIF
  ALLOCATE(RMonteCarloArray2(IMonteCarloIterations,K),STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RMonteCarloArray2 ALLOCATE" 
  ENDIF
  ALLOCATE(RRandomCol(K),STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RRandomCol ALLOCATE"
  ENDIF
  ALLOCATE(RRandomRow(IMonteCarloIterations),STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RRandomRow ALLOCATE"
  ENDIF 
  ALLOCATE(RSensitivity(IMonteCarloIterations),STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Allocate of RSensitivity"
  END IF
  
  ALLOCATE(RSpecificity(IMonteCarloIterations),STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Allocate of RSpecificity"
  END IF
  
  ALLOCATE(RCutoffs(IMonteCarloIterations,K),STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Allocate of RCutoffs"
  END IF
  
  !! BUILD LOOK UP TABLE
  
  DO ind = 1,K
     DO jnd = 1,N
        ILookup_Table(jnd,ind) = NCHOOSEK(jnd,K-(ind-INT(1.0,IKIND)))
     END DO
  END DO
  
  !! PARALLELISATION VARIABLES

  IMaxcombinations = NCHOOSEK(N,K); ! Total Combinations to calculate for
  ICombinationSum = 0;
  ICount_global = 0;

  !  Calculate Max and Mins
  DO jnd = 1,N
     RVectorMin(jnd) = MINVAL(RAntigen_Measurements(:,jnd))
     IMAXLOC = MAXLOC(RAntigen_Measurements(:,jnd),DIM=1)
     RVectorMax(jnd) = 0.0
     DO knd = 1,IPatients
        IF(knd.NE.IMAXLOC)THEN
           IF(RAntigen_Measurements(knd,jnd)>RVectorMax(jnd)) THEN
              RVectorMax(jnd) = RAntigen_Measurements(knd,jnd) 
           END IF
        END IF
     END DO
  END DO
   
  ! Initialise Result Arrays
  RSensitivity = 0.0
  RSpecificity = 0.0
  RCutoffs = 0.0
  ISuccess = 0

  CALL RANDOM_NUMBER(RMonteCarloArray)

  !! Decide if restart or not
  
  IF(RESTART)THEN
     Start = RestartMatrix(1)
     ICount_local = RestartMatrix(2)
     ISuccessRate = RestartMatrix(3)
     IProgress = RestartMatrix(4)
  ELSE
     Start = MY_RANK
     ICount_local = 0;
     ISuccessRate = 0
     IProgress = 1;
  END IF

  !! Set output files
  
  write (OUTFILE,"(A17,I4.4)") "Results/MonteOut_",MY_RANK
  OPEN(unit=2*(MY_RANK+1)*4,FILE=trim(OUTFILE),STATUS="UNKNOWN",IOSTAT=IERR)
!   WRITE(ANTIGENFORMAT,'("(I12,1X,F5.3,1X,F5.3,1X,",I0,"(F10.3,1X))")') K
  WRITE(COMBINATIONFORMAT,&
	'("(I20,1X,",I0,"I3,1X,F5.3,1X,F5.3,1X,",I0,"(F10.3,1X)))")') &
	K,K

  !! Begin combination calculation
  
  DO ind = Start,IMaxcombinations,PROCESSES
     IF (ind.LT.IMaxcombinations) THEN
        ! CALCULATE COMBINATION 
        ICount_local = ICount_local + 1
        ICombinationSum = 0
        DO jnd = 1,K
           ICombination(jnd) = N
           ILargest_Vals(jnd) = 0
           DO knd = 1,N
              IF(ILookup_Table(knd,jnd).GT.(ind-ICombinationSum)) THEN
                 EXIT
              ELSE
                 ILargest_Vals(jnd) = ILookup_Table(knd,jnd)
                 ICombination(jnd) = N-knd
              END IF
           END DO
           ICombinationSum = ICombinationSum + ILargest_Vals(jnd)
        END DO

        ! SCIENCE BIT
        ILowerlim = MOD(ICount_local,99*K)
        RMonteCarloArray2 = RMonteCarloArray(:,(ILowerlim+1):(ILowerlim+K))
        DO jnd = 1,K
           RMonteCarloArray2(:,jnd) = ((RVectorMax(ICombination(jnd)) - RVectorMin(ICombination(jnd))) * &
                RMonteCarloArray2(:,jnd)) + RVectorMin(ICombination(jnd))
        END DO

        RSensitivity = 0.00;
        RSpecificity = 0.00;
        ISuccess = 0
        CALL Sensitivity(&
             RAntigen_Measurements(:,ICombination),RMonteCarloArray2,ILabels,&
             RSensitivity,RSpecificity,RCutoffs,&
             IMonteCarloIterations,K,IPatients,ISuccess,IERR)
			 
        IWrite = 0
        
        IF (ISuccess.GT.0) THEN
           IF(MAXVAL(RSensitivity).GT.MINSEN.AND.MAXVAL(RSpecificity).GT.MINSPEC) THEN 
              DO jnd = 1,ISuccess
                 IF(RSensitivity(jnd).GT.MINSEN.and.RSpecificity(jnd).GT.MINSPEC)THEN
                    IWrite = 1
                 END IF
              END DO
              IF(IWrite.eq.1) THEN
                 DO jnd = 1,ISuccess             
                    IF(RSensitivity(jnd).GT.MINSEN.and.RSpecificity(jnd).GT.MINSPEC)THEN
                       IF(MY_RANK.EQ.0) THEN
                          WRITE(*,*) RSensitivity(jnd),RSpecificity(jnd)
                       END IF
                       WRITE(UNIT=2*(MY_RANK+1)*4,FMT=trim(COMBINATIONFORMAT),IOSTAT=IERR) &
                            ind,(ICombination(knd),knd=1,K),&
                            RSensitivity(jnd),RSpecificity(jnd),(RCutoffs(jnd,knd),knd=1,K)
                       ISuccessRate = ISuccessRate + 1
                    END IF
                 END DO
              END IF
           END IF
        END IF

        IF(ISuccessRate.EQ.MINFILES) THEN
           EXIT
        END IF
        
        IF (ICount_local.EQ.INT(&
             (REAL(IMaxcombinations,RKIND)/REAL(PROCESSES,RKIND))*&
             (REAL(IProgress,RKIND)/REAL(100,RKIND)),IKIND)) THEN
           IF (MY_RANK.EQ.0) THEN
              WRITE(*,*) "(",MY_RANK,") Thats",IProgress,"%"
           END IF
           IProgress = IProgress + 1
           !! Save Restart Information
           write (OUTFILE,"(A13,I4.4)") "Restart/Save_",MY_RANK 
           OPEN(unit=(MY_RANK+1)*4,FILE=trim(OUTFILE),STATUS="UNKNOWN",IOSTAT=IERR)
           Write(unit=(MY_RANK+1)*4,FMT='(I20,1X,I20,1X,I4,1X,I4)') ind,ICount_local,ISuccessRate,IProgress
           CLOSE(unit=(MY_RANK+1)*4,IOSTAT=IERR)           
        END IF
     END IF
  END DO

  close(unit=2*(MY_RANK+1)*4)
  
  !! END TIMING

  CALL CPU_TIME(end_time)
  time_local = end_time-start_time
 
  !! FINAL COMMS

  CALL MPI_ALLREDUCE(time_local,time_global,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERR)
  CALL MPI_ALLREDUCE(ICount_local,ICount_global,1,MPI_DOUBLE_PRECISION,MPI_SUM,MPI_COMM_WORLD,IERR)
  IF (MY_RANK.GE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Done, COUNT =",ISuccessRate,REAL(ISuccessRate,RKIND)/REAL(ICount_local,RKIND),&
          ICount_local,ICount_global,time_local,"seconds",time_global,"seconds"   
  END IF
  
  !! DEALLOCATE MEMORY

  DEALLOCATE(ILookup_Table,STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on LOOKUP DEALLOCATE"
  ENDIF
  DEALLOCATE(ILargest_Vals,STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on SET DEALLOCATE"
  ENDIF
  DEALLOCATE(ICombination,STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on SET DEALLOCATE"
  ENDIF
  DEALLOCATE(RAntigen_Measurements,STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on Antigen_Measurements DEALLOCATE"
  ENDIF
  DEALLOCATE(RVectorMin,STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RVectorMin DEALLOCATE"
  ENDIF
  DEALLOCATE(RVectorMax,STAT=IERR)
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RVectorMax DEALLOCATE" 
  ENDIF
  DEALLOCATE(RScan_Matrix,STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RScan_Matrix DEALLOCATE" 
  ENDIF
  DEALLOCATE(RMonteCarloArray,STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RMonteCarloArray DEALLOCATE" 
  ENDIF
  DEALLOCATE(RMonteCarloArray2,STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RMonteCarloArray2 DEALLOCATE" 
  ENDIF
  DEALLOCATE(RRandomCol,STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RRandomCol DEALLOCATE" 
  ENDIF
  DEALLOCATE(RRandomRow,STAT=IERR) !! ALLOCATION NEEDS TO BE DYNAMIC
  IF (IERR.ne.0) THEN
     write(*,*) "(",MY_RANK,") Error on RRandomRow DEALLOCATE" 
  ENDIF
  DEALLOCATE(RSensitivity,STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Deallocate of RSensitivity"
  END IF
  DEALLOCATE(RSpecificity,STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Deallocate of RSpecificity"
  END IF
  DEALLOCATE(RCutoffs,STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Deallocate of RCutoffs"
  END IF
  DEALLOCATE(ILabels,STAT=IERR)
  IF (IERR.NE.0) THEN
     WRITE(*,*) "(",MY_RANK,") Error on Deallocate of ILabels"
  END IF

  !! MPI FINALISE

  CALL MPI_FINALIZE(IERR)
  IF (IERR.ne.0) THEN
     write(*,*) 'Error on MPI_FINALISE'
  ENDIF
  
end program Antigens
