%% this functions refilters results with higher a sensitivity and specificites

clear all
close all

p = gcp('nocreate');
if isempty(p)
    parpool
end

tic

%% USER AREA

Cores = 192;
PATH = '/home/kidwhizz/Documents/montecarlo/51ch8/';
ResultsFolder = 'Results';
NewSens = 0.6;
NewSpec = 0.8;

%% WORK AREA NO TOUCHY

if ~isunix
    strrep(PATH,'/','\')
    strrep(ResultsFolder,'/','\')
    cd([PATH ResultsFolder])
    filteredResultsFolder = [ResultsFolder '_filtered\'];
    if ~exist([PATH filteredResultsFolder],'file')
        mkdir([PATH filteredResultsFolder])
    end
else
    cd([PATH ResultsFolder])
    filteredResultsFolder = [ResultsFolder '_filtered/'];
    if ~exist([PATH filteredResultsFolder],'file')
        mkdir([PATH filteredResultsFolder])
    end
end

%% Read in files and filter

parfor i = 1:Cores
    filtered = false;
    Listing = dir(['MonteOut_*_' num2str(i-1,'%04.f')]);
    Combinations = dlmread(['MonteOut_' num2str(i-1,'%04.f')]);
    for j = 1:size(Listing,1)
        DataTemp = load(Listing(j).name);
        Data_filtered = DataTemp(DataTemp(:,1) >= NewSens & DataTemp(:,2) >= NewSpec,:);
        if(~isempty(Data_filtered))
            dlmwrite([PATH filteredResultsFolder Listing(j).name],Data_filtered,'delimiter','\t');
            [str,count] = sscanf(Listing(j).name,'%9c%d%1c%d%1c%d');
            if exist([PATH filteredResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],'file')
                dlmwrite([PATH filteredResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],Combinations(Combinations(:,1)==str(12),:),'delimiter','\t','-append','precision',['%12d' '%' num2str(size(Combinations,2)-1) 'd'])
            else
                dlmwrite([PATH filteredResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],Combinations(Combinations(:,1)==str(12),:),'delimiter','\t','precision',['%12d' '%' num2str(size(Combinations,2)-1) 'd'])
            end
        end
    end
end  
toc