# My project's README

Requires :

GCC
OPENMPI or MPICH

Command Line Arguements :

There is 1 required command line arguement :

Input Filename

As well as 1 optional one

RESTART

included as the 2nd arguement and the code will continue from the previous position and will require the folder /Restart/ inorder to do so

There are 3 input files creates using with the matlab preprocessor

X.antigens
X.labels
x.param

these contain the antigen counts for each patient, the labels for each patient (.antigen and .labels respectively) while the .param file contains the number of antigens, minimum number of antigens, the number of montecarlo iteations, the minimum sensitivities and specicitities and the number of files to output per core.

These values are chosen using the matlab preprocessor for to make the program input more simple