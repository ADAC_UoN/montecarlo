clear all
close all

%% USER INPUT AREA %%%%%%%

filename = 'DataForKeith_Interpolated_40Antigens';
MinimumAntigens = 8;
MonteCarloIterations = 50000;
MinimumSensitivity = 0.5;
MinimumSpecificity = 0.7;
MaximumNumberofOutputfiles = 7000;

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DATA READ IN

[data,txt,Main] = xlsread([filename,'.xlsx']);
labels = data(:,size(data,2));
data(:,size(data,2)) = [];

% create example data
ydata = data';
[r, c] = size(ydata);
xdata = repmat(1:c, r, 1);
% for explanation se
% http://undocumentedmatlab.com/blog/undocumented-scatter-plot-jitter
nfig = 1;
figure(nfig)
% nfig = nfig+1;
scatter(xdata(:), ydata(:), 'r.', 'jitter','on', 'jitterAmount', 0.05);
hold on;
plot([xdata(1,:)-0.15; xdata(1,:) + 0.15], repmat(mean(ydata, 1), 2, 1), 'k-')
ylim([0 max(ydata(:)+1)])

antigen_measurements = data;

% Creating an array of antigen names
Antigens = Main(1,1:end-1)';

dlmwrite([filename '.antigens'],antigen_measurements);
dlmwrite([filename '.labels'],labels);
dlmwrite([filename '.param'],...
    [size(antigen_measurements,2); MinimumAntigens; MonteCarloIterations; MinimumSensitivity; MinimumSpecificity; MaximumNumberofOutputfiles]);
