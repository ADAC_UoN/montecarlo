% reads output and displays unique sensitivity/specificity combinations

clear all
close all
tic

%% USER AREA

PATH = '/home/kidwhizz/Documents/montecarlo/results/39ch8/';
ResultsFolder = 'Results/';

Cores = 128;

%% END OF USER AREA

%% NO TOUCHY BEYOND THIS POINT

if ~isunix
    strrep(PATH,'/','\')
    strrep(ResultsFolder,'/','\')
    cd([PATH ResultsFolder])
else
    strrep(PATH,'\','/')
    strrep(ResultsFolder,'\','/')
    cd([PATH ResultsFolder])
end

Successes = 0;

%% Read in files
for i = 1:Cores
    Listing = dir(['MonteOut_' num2str(i-1,'%04.f')]);
    for j = 1:size(Listing,1)
        DataTemp = load(Listing(j).name);
        Successes = Successes + size(DataTemp,1);
    end
end

Antigens = (size(DataTemp,2)-3)/2; % output file lines contain 2 values for each antigen + 3 more for the sensitivity, specificity, 

Data = zeros(Successes,2);
Successes = 1;

for i = 1:Cores
    Listing = dir(['MonteOut_' num2str(i-1,'%04.f')]);
    for j = 1:size(Listing,1)
        DataTemp = load(Listing(j).name);
        Data(Successes:(Successes+size(DataTemp,1)-1),:) = DataTemp(:,(Antigens+2):(Antigens+3));
        Successes = (Successes+size(DataTemp,1)) ;
    end
end

%% Find Unique entries and count occurances
Data2 = unique(Data,'rows');

Data3 = zeros(size(Data2,1),1);
for i = 1:size(Data2,1)
    for j = 1:size(Data,1)
        if(Data(j,:)==Data2(i,:))
            Data3(i) = Data3(i) + 1;
        end
    end
end
%% plot

subplot(1,3,1),scatter(Data2(:,1),Data2(:,2),1000,ones(size(Data2,1),1),'filled')
xlim(gca,[min(Data(:,1))-(range(Data2(:,1))*0.1) max(Data(:,1))+(range(Data2(:,1))*0.1)])
ylim(gca,[min(Data(:,2))-(range(Data2(:,2))*0.1) max(Data(:,2))+(range(Data2(:,2))*0.1)])
xlabel 'Sensitivity'
ylabel 'Specificity'
title 'Combination Results'
grid on

set(gcf,'Position',[0 0 1800 450])

subplot(1,3,2),scatter(Data2(:,1),Data2(:,2),1000,log10(Data3),'filled')
xlim(gca,[min(Data(:,1))-(range(Data2(:,1))*0.075) max(Data(:,1))+(range(Data2(:,1))*0.075)])
ylim(gca,[min(Data(:,2))-(range(Data2(:,2))*0.1) max(Data(:,2))+(range(Data2(:,2))*0.1)])
xlabel 'Sensitivity'
ylabel 'Specificity'
title 'Combination Heat Map'
grid on
colorbar

%% read and plot all combinations

MaxAntigens = 0;

for i = 1:Cores % count antigens to find maximum
    if isunix
        if exist([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],'file')
            Combinations = load([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')]);
        end
    else
        if exist([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],'file')
            Combinations = load([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')]);
        end
    end
    for j = 1:size(Combinations,1)
        MaxAntigens = max([Combinations(j,2:(Antigens+1)) MaxAntigens]);
    end
end

Statistics = zeros(MaxAntigens,1);

for i = 1:Cores
    if isunix
        if exist([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],'file')
            Combinations = load([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')]);
        end
    else
        if exist([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')],'file')
            Combinations = load([PATH ResultsFolder 'MonteOut_' num2str(i-1,'%04.f')]);
        end
    end
    for j = 1:size(Combinations,1)
        Statistics(Combinations(j,2:(Antigens+1))) = Statistics(Combinations(j,2:(Antigens+1))) + 1;
    end
end

%% plot stats
Statistics = 100*Statistics/size(Data,1);
subplot(1,3,3),scatter(1:MaxAntigens,(Statistics))
xlim([0 MaxAntigens+1])
ylabel 'Occurances'
xlabel 'Antigen'
title 'Antigen Occurance'
grid on
%%
toc
