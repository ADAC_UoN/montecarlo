% This function reformats older output which were named
% "MonteOut_[FileNumber]_[Combination ID]_[Core]" into
% "MonteOut_[Core]_[Combination ID]_[FileNumber]"
  
clear all
close all

p = gcp('nocreate');
if isempty(p)
    parpool
end

tic

%% USER AREA

PATH = '/home/kidwhizz/Documents/montecarlo/51ch8/';
ResultsFolder = 'Results_filtered/';
Cores = 192;

%% NO TOUCHY

if ~isunix
    strrep(PATH,'/','\')
    strrep(ResultsFolder,'/','\')
    if ~isempty(strfind(ResultsFolder,'\'))
        ResultsFolder = ResultsFolder(1:(strfind(ResultsFolder,'\')-1));
    end
    cd([PATH ResultsFolder])
else
    if ~isempty(strfind(ResultsFolder,'/'))
        ResultsFolder = ResultsFolder(1:(strfind(ResultsFolder,'/')-1));
    end
    cd([PATH ResultsFolder])
end

parfor i = 1:Cores
    if exist(['MonteOut_' num2str(i-1,'%04.f') ],'file')
        CoreListing = load(['MonteOut_' num2str(i-1,'%04.f') ],'-ascii');
        Listing = dir(['MonteOut_*_' num2str(i-1,'%04.f')]);
        for j = 1:size(Listing,1)
            DataTemp = load(Listing(j).name);
            if isunix
                if ~exist([PATH ResultsFolder '_reformatted'],'file')
                    mkdir([PATH ResultsFolder '_reformatted']);
                end
                dlmwrite([[PATH ResultsFolder '_reformatted'] '/MonteOut_' num2str(i-1,'%04.f') '_' num2str(CoreListing(j,1),'%012.f') '_' num2str(j-1,'%04.f')],[CoreListing(j)*ones(size(DataTemp,1),1),DataTemp],'delimiter','\t','precision',12)
            else
                if ~exist([PATH ResultsFolder '_reformatted'],'file')
                    mkdir([PATH ResultsFolder '_reformatted']);
                end
                dlmwrite([[PATH ResultsFolder '_reformatted'] '\MonteOut_' num2str(i-1,'%04.f') '_' num2str(CoreListing(j,1),'%012.f') '_' num2str(j-1,'%04.f')],[CoreListing(j)*ones(size(DataTemp,1),1),DataTemp],'delimiter','\t','precision',12)
            end
        end
        copyfile(['MonteOut_' num2str(i-1,'%04.f') ],[PATH ResultsFolder '_reformatted']);
    end
end

toc
